define([
    'intern!object',
	'intern/chai!assert',
	'routes/dummy'
], function (registerSuite, assert, dummy) {
	registerSuite({
		name: 'tests for functions of onsubmit.js',

		getValidText: function() {
			assert.strictEqual(dummy.getValidText('michael@schumacher.com'), 'michael@schumacher.com',
				'should return the same value');
			assert.strictEqual(dummy.getValidText(undefined), '',
				'should return empty string as input is undefined');
		},
		getValidEmailAddress: function() {
			assert.strictEqual(dummy.getValidEmailAddress('michael@schumacher.com'), 'michael@schumacher.com',
				'should return the same value');
			assert.strictEqual(dummy.getValidEmailAddress(undefined), '',
				'should return empty string as input is undefined');
			assert.strictEqual(dummy.getValidEmailAddress('michael'), '',
				'should return empty string as input is invalid');
		},
		getValidEmailAddresses: function() {
			assert.equal(dummy.getValidEmailAddresses('michael@schumacher.com'), 'michael@schumacher.com',
				'should return the same value - array with one element');
			assert.equal(dummy.getValidEmailAddresses('mic-h.a_el@schumacher.com'), 'mic-h.a_el@schumacher.com',
				'should return the same value - array with one element, dot, underscore, hyphen');
			assert.equal(dummy.getValidEmailAddresses('mic-h.a_el@schumache_r.com'), '',
				'should return empty string as underscore is after @ in the domain name which is invalid');
			assert.equal(dummy.getValidEmailAddresses(' @schumache_r.com'), '',
				'should return empty string as there is no user name before @');
			assert.equal(dummy.getValidEmailAddresses('c@t'), '',
				'should return empty string as email address is invalid');
			assert.equal(dummy.getValidEmailAddresses('c@t.'), '',
				'should return empty string as email address is invalid');
			assert.equal(dummy.getValidEmailAddresses('c@t.c'), '',
				'should return empty string as email address is invalid');
			assert.equal(dummy.getValidEmailAddresses('mic-h.a_el@schumache-r.com'), 'mic-h.a_el@schumache-r.com',
				'should return the same string');
			assert.equal(dummy.getValidEmailAddresses('mic-h.a_el@schumache-r.co.im'), 'mic-h.a_el@schumache-r.co.im',
				'should return the same string');
			assert.equal(dummy.getValidEmailAddresses('mic-h.a_el@schumache-r.co.im '), 'mic-h.a_el@schumache-r.co.im',
				'should return the same string despite trailing whitespaces');
			assert.equal(dummy.getValidEmailAddresses('michael@schumacher.com, abc@xyz.com'), 'michael@schumacher.com,abc@xyz.com',
				'should return the same value - array with two elements element');
			assert.equal(dummy.getValidEmailAddresses('michael@schumacher.com - abc@xyz.com'), '',
				'should return the empty string - array with two elements element separated by -');
			assert.equal(dummy.getValidEmailAddresses('michael@schumacher.comabc@xyz.com'), '',
				'should return empty string as input params are not separated by commas');
			assert.equal(dummy.getValidEmailAddresses('michael,schumacher,comabc,xyz.com'), '',
				'should return empty string as all input params are invalid');
			assert.equal(dummy.getValidEmailAddresses('michael@schumacher.com, abc'), 'michael@schumacher.com',
				'should return one less element as it is invalid - array with one element');
			assert.equal(dummy.getValidEmailAddresses(undefined), '',
				'should return empty string as input is undefined');
			assert.equal(dummy.getValidEmailAddresses('michael'), '',
				'should return empty string as input is invalid');
		},
		getValidFileName: function() {
			assert.equal(dummy.getValidFileName('filename.pdf'), 'filename.pdf',
				'should return the same value');
			assert.equal(dummy.getValidFileName('beb6d7c4#$%^&*()_-=@!@#$%^&ca75a1229240f6d161c8ce68.pdf'), 'beb6d7c4#$%^&*()_-=@!@#$%^&ca75a1229240f6d161c8ce68.pdf',
				'should return the same value despite odd characters');
			assert.equal(dummy.getValidFileName(undefined), '',
				'should return empty string as input is undefined');
		},
		getValidFilePath: function() {
			assert.equal(dummy.getValidFilePath('/user/abc/xyz/filename.pdf'), '/user/abc/xyz/filename.pdf',
				'should return the same value');
			assert.equal(dummy.getValidFilePath('/user/abc/xyz/../filename.pdf'), '',
				'should return empty string as input string contains access to parent directory by using ..');
			assert.equal(dummy.getValidFilePath(undefined), '',
				'should return empty string as input is undefined');
		}
	});
});
