/**
* The main method which handles the http request directed
* at the '/' root page
*
* @param {Http Response} res An Http response object
* @param {Http Request} req An Http request object
*/
exports.doWork = function(req, res){
	console.log("in homepage.js");
	res.render('home', function(err, html) {
		if (err) {
             console.log(err); 
         }
         else { 
             res.send(html)
         };
    });
};