/**
* Sends the email message using the SendGrid API
*
* @method trySendGrid
* @param {Map} map A map object containing email params to be sent
* @param {String} userid Userid for the SendGrid account
* @param {String} pwd Password for the SendGrid account
* @param {Function} callback A callback function on the boolean object
* @return {Boolean} Calls the callback method with the bollean value to be 
* returned
*/
function trySendGrid(map, userid, pwd, callback) {
	var sendgrid  = require('sendgrid')(userid, pwd); // TODO remove credentials
    
	/* Preparing the email to be sent */
	var email = new sendgrid.Email();
	email.setFrom(map['from']);
	email.setTos(map['to']);
	if(map['cc'].length > 0) {email.setCcs(map['cc']);};
	if(map['bcc'].length > 0) {email.setBccs(map['bcc']);};
	email.setSubject(map['subject']);
	email.setText(map['mailbody']);
	if(map['filePath']) {
		email.addFile({
			path: map['filePath']
		});
	}
    
	/* Sending the email */
	try {
		sendgrid.send(email,
			function(err, json) {
			if (err) { console.log("Oops! We couldn't send the email using"+
				"SendGrid!");
				console.log("This was the error message: " + err); // TODO 
				callback(false);
			} else {
				console.log("Email successfully sent using SendGrid!");
				console.log(json);
				callback(true);
			}
		});
	}
	catch(err) {
		console.log("ERROR! Function will return from the catch block now! "+err);
		callback(false);
	}
}

/**
* Sends the email message using the MailGun API
*
* @method tryMailGun
* @param {Map} map A map object containing email params to be sent
* @param {String} apiKey API key for the MailGun account
* @param {String} domain Domain name registered for with the MailGun account
* @param {Function} callback A callback function on the boolean object
* @return {Boolean} Calls the callback method with the bollean value to be 
* returned
*/
function tryMailGun(map, apiKey, domain, callback) { 
	var Mailgun = require('mailgun-js');
	var pathApi = require('path');
	var mailgun = new Mailgun({apiKey: apiKey, domain: domain});

	/* Preparing the email to be sent */
	var data = {
		from: map['from'],
		to: map['to'],
		subject: map['subject'],
		text: map['mailbody'],
	};

	if(map['cc'].length > 0) {data['cc'] = map['cc'];};
	if(map['bcc'].length > 0) {data['bcc'] = map['bcc'];};
	if(map['filePath']) {
		var fp = pathApi.join(map['filePath']);
		data['attachment'] = fp;
	}

	/* Sending the email */
	try {
		mailgun.messages().send(data, function (err, body) {
			if (err) {
				console.log("Oops! We couldn't send the email using MailGun!");
				console.log("This was the error message: " + err); // TODO 
				callback(false);
			} else {
				console.log("Email successfully sent using MailGun!");
				callback(true);
			}
		});
	}
	catch(err) {
		console.log("ERROR! Function will return from the catch block now! "+err);
		callback(false);
	}
}

/**
* Returns a list of valid/well-formed email addresses out of the list of email 
* address sent to this method. Returns an empty list if none of the emails were
* valid
*
* @method getValidEmailAddresses
* @param {List} rawEmailsList A list of email addresses which were entered by 
* the user
* @return {List} legalEmails A list of email addresses which are 
* valid/well-formed
*/
function getValidEmailAddresses(rawEmailsList) {
	if(typeof rawEmailsList === 'undefined') {
		return '';
	}
	var parsedEmailsList = rawEmailsList.split(","); var legalEmails = [];
	for (var i = 0; i < parsedEmailsList.length; i++) {
		var rawEmail = getValidEmailAddress(parsedEmailsList[i]);
		if(rawEmail) {
			legalEmails.push(rawEmail);
		}
	}
	return legalEmails;
}

/**
* Returns the email address it received if it was valid/well-formed or returns 
* an
* empty string otherwise
*
* @method getValidEmailAddress
* @param {String} rawEmailsList A list of email addresses which were entered by 
* the user
* @return {String} the email address it received if it was valid/well-formed or 
* returns an empty string otherwise
* @link 
* http://stackoverflow.com/questions/46155/validate-email-address-in-javascript
* The regex in this method has been taken from this link
*/
function getValidEmailAddress(rawEmail) {
	if(typeof rawEmail === 'undefined') {
		return '';
	}
	var rawEmail = rawEmail.trim();
	var regex = 
	/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	if(regex.test(rawEmail)) {
		return rawEmail;
	} else {
		return '';
	}
}

/**
* Returns the raw text it received or returns an empty string if original string 
* was undefined
*
* @method getValidText
* @param {String} rawText A string entered by the user
* @return {String} the raw text it received or returns an empty
* string if original string was undefined
*/
function getValidText(rawText) {
	if(typeof rawText === 'undefined') {
		return '';
	}
	return rawText;
}

/**
* Returns the raw file name it received or returns an empty string if original 
* string was undefined
*
* @method getValidFileName
* @param {String} rawFileName A string entered by the user
* @return {String} the raw file name it received or returns an empty
* string if original string was undefined
*/
function getValidFileName(rawFileName) {
	if(typeof rawFileName === 'undefined') {
		return '';
	}
	var fileName = rawFileName.originalname;
	return fileName;
}

/**
* Returns the raw file path it received if it it doesn't contain a '..'
* which gives him an access to the parent directory or returns an empty string 
* if original string was undefined / illegal
*
* @method getValidFilePath
* @param {String} rawFilePath A string entered by the user
* @return {String} the raw file path it received or returns an empty
* string if original string was undefined / illegal
*/

function getValidFilePath(rawFilePath) {
	if(typeof rawFilePath === 'undefined') {
		return '';
	} else {
		var filePath = rawFilePath.path;
		if(filePath.indexOf("..") !== -1) {
			return '';
		} else {
			return __dirname + '/../' + filePath;
		}
	}
}

/**
* Logs the details of the map it received
*
* @method logMessageDetails
* @param {Map} map A map of key value mappings holding the email params
* These logging details could be written to a file or any other output stream as 
* needed 
*/
function logMessageDetails(map) {
	console.log("These are the details of the message which is about to be sent");
	console.log("\tFrom: "+map['from']);
	console.log("\tTo Emails: ");
	console.log(map['to']);
	console.log("\tCC Emails: ");
	console.log(map['cc']);
	console.log("\tBCC Emails: ");
	console.log(map['bcc']);
	console.log("\tSubject: "+map['subject']);
	console.log("\tBody: "+map['mailbody']);
	console.log("\tFile Name: "+map['fileName']);
	console.log("\tFile Path: "+map['filePath']);
}

/**
* Calls the success page if the email was sent succcessfully
*
* @method renderSuccessPage
* @param {Http Response} res An Http response object
*/
function renderSuccessPage(res) {
	console.log('Success -- the email has been sent successfully so rendering'+
		'the success page now');
	res.render('submitted');
}

/**
* Calls the failure page if the email delivery failed
*
* @method renderFailurePage
* @param {Http Response} res An Http response object
*/
function renderFailurePage(res) {
	console.log('Failure -- the email could not be sent so rendering the failure'+
		'page now');
	res.render('error');
}

/**
* The main method which handles the http request directed at the '/onsubmit' 
* page
*
* @param {Http Response} res An Http response object
* @param {Http Request} req An Http request object
*/
exports.doWork = function(req, res){
	if (!req.body) return res.sendStatus(400);

	var map = new Object();
	map['from'] = getValidEmailAddress(req.body.from);
	map['to'] = getValidEmailAddresses(req.body.to);
	map['cc'] = getValidEmailAddresses(req.body.cc);
	map['bcc'] = getValidEmailAddresses(req.body.bcc);
	map['subject'] = getValidText(req.body.subject);
	map['mailbody'] = getValidText(req.body.mailbody);
	map['fileName'] = getValidFileName(req.files.file);
	map['filePath'] = getValidFilePath(req.files.file);

	logMessageDetails(map);

	console.log('We will first try to send the email using SendGrid and if that'+
		'does not work then we will try it using MailGun!');
	trySendGrid(map, 'userid', 'password', function(success){
		if(success) {
			renderSuccessPage(res);
		} else {
			tryMailGun(map, 'your-key', 
			'your-domain.com', function(success){
				if(success) {
					renderSuccessPage(res);
				} else {
					renderFailurePage(res);
				}
			});
		}
	});
};
