/*
 * A very simple AMD module with no dependencies
 */
 
define([], function () {
	return {
		getValidText: function (rawText) {
	    	if(typeof rawText === 'undefined') {
				return '';
			}
			return rawText;
        },
        getValidEmailAddress: function (rawEmail) {
	    	if(typeof rawEmail === 'undefined') {
				return '';
			}
			var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		    if(regex.test(rawEmail)) {
		        return rawEmail;
		    } else {
		        return '';
		    }
        },
        getValidEmailAddresses: function (rawEmailsList) {
	    	if(typeof rawEmailsList === 'undefined') {
		        return '';
		    }
		    /* This converts raw String of emails into an array of emails 
		     * A better mechanism could be put in place from the front end */
		    var parsedEmailsList = rawEmailsList.split(","); 
		    var legalEmails = [];
		    var email = ""; 
		    var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		    for (var i = 0; i < parsedEmailsList.length; i++) {
		        email = parsedEmailsList[i];
		        email = email.trim();
		        if(regex.test(email)) {
		            legalEmails.push(email);
		        }
		    }
		    console.log("legalEmails being returned from the function: "+legalEmails);
		    return legalEmails;
        },
        getValidFileName: function (fileName) {
		    if(typeof fileName === 'undefined') {
		        return '';
		    }
		    console.log("fileName being returned from the function: "+fileName);
		    return fileName;
		},
		getValidFilePath: function (filePath) {
		    if(typeof filePath === 'undefined') {
		        return '';
		    } else {
		        if(filePath.indexOf("..") !== -1) {
		            return '';
		        } else {
		            return filePath;
		        }
	    	}
	    }
	};
});