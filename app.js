// Importing basic modules required for this project
var express = require('express');
var multer  = require('multer')
var http = require('http');
var path = require('path');

var homepage = require('./routes/homepage');
var onsubmit = require('./routes/onsubmit');

// Initializing express
var app = express();

// If the port env variable is not set then use 8080 as the default
// Two ways to set the port env variable, 1. $ PORT=1234 node app.js 
// (temporarily)
// 2. $ export PORT=1234 (permanently)
app.set('port', process.env.PORT || 8080);

// Using the Jade as template engine - neat & clean
app.set('view engine', 'jade');
app.use(multer({ dest: './uploads/'}))

// Starting the server
http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

// Specifying how to handle requests arriving on various paths
app.get('/', homepage.doWork);
app.post('/onsubmit', onsubmit.doWork);
