This repo is created to track the project created as part of the Uber take home code challenge.

Email Service

Create a service that accepts the necessary information and sends emails. It should provide an abstraction between two different email service providers. If one of the services goes down, your service can quickly failover to a different provider without affecting your customers.

I have opted for the Back-end track for this task as I am applying for the Back-end Software Engineering position. I opted for this particular challenge because out of all the challenges, this seemed the best suited to me where I can spend minimal effort on front-end and focus solely on the under-the-hood backend stuff to handle this email sending task. 

I have used SendGrid and MailGun APIs to send the emails. Apart from learning how to send emails using these two APIs by referring to their respective websites, I haven't used any other (boiler-plate) code from anywhere else. Everything is written by me. Front-end code was written as shown in [https://stormpath.com/blog/build-nodejs-express-stormpath-app/](https://stormpath.com/blog/build-nodejs-express-stormpath-app/) tutorial

To build this Email app, I have used the Node.js framework. I had previously used Node.js (and JavaScript) while building a Pinterest like pin-board photo tagging web-app. This is pretty much it regarding my previous experience to this language and framework. I've done most of my project mostly in Java. But man this callback is a beautiful thing! :-)
Apart from this, my reasons for selecting Node.js framework are as follows,<br>
	1. Great frame-work for building webapps with event driven methodology, V8 engine and overall neat and lightweight structure<br>
	2. Node package manager<br>
	3. Ample number of modules available through node package manager which can be extended to your app<br>
	4. Easily scalable, fast and efficient (no use of threads)<br>
	5. Simple and clean project structure/architecture<br>
	6. (low priority reason) I have already written an API style project (not a web app) in Java for BinaryTrees which is hosted on Github (https://github.com/dhyanamvaidya/BinaryTree). So this time I (preferably) wanted to use some other language and framework. And since JavaScript and Node.js are used heavily at Uber as well, this turned out to be a win-win solution! :-) <br>

As I haven't done any prior Unit Testing in JavaScript, it was a bit tricky to search for a JUnit style testing framework. So eventually I have used Intern (https://theintern.github.io/intern/#what-is-intern) for this purpose. I was not able to fully figure out how to use functions of an existing .js class in testing so I have created a dummy.js class under the tests folder which holds all the methods which had to be tested. They have been tested throughly and attain 100% branch, function and statement coverage and all the test cases pass successfully. The only two methods which haven't been tested are trySendGrid() and tryMailGun() from the routes/onsubmit.js. As these methods directly send an email once they are executed, I haven't tested them. But stay rest assured, I have done a lot of testing from the web browser for these two functions. Also, all the parameters which are passed to this functions are totally error-free and heavily tested. So after that, these methods work fine.

The failsafe mechanism of falling back to MailGun when email cannot be sent using SnedGrid and vice-versa works very well. I could easily test this mechanism by preparing an email which just has From, To and Message-body fields. Sending such an email using SendGrid causes an error because its API requires an email to specify a Subject. But MailGun API doesn't require that. So even though such an email could not be sent using SendGrid, MailGun comes to the rescue! (phew!)

Another point I considered was that Amazon's API had specified that emails cannot be sent to more than 50 recipients in one attempt. SendGrid and MailGun haven't specified any such restrictions and as sending an email to more than 50 recipients would be a 'Spam!' on my behalf, I haven't tested that aspect.

At times, I had seen email messages sent through SendGrid or MailGun directed directly to the Spam folder by Gmail. There had been delay in receiving the messages as well but emails did arrive (better late than never!)

I have passed the parameters entered by the user from the browser to the appropriate .js file using the HTTP-POST method. I have printed all the logging statements directly to the console which can easily be configured to be sent somewhere else (like a file etc) as required.

This basically sums up all of my background and design decisions. Thanks for reading! :-)